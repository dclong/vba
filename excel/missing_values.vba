Sub blank()
    Rem:------------------------------------------------
    'Convert blank values to ...
    'considering ask user for input of fill value
    Rem:-----------------------------------------------
    For i = 2 To UsedRange.Rows.Count
        For j = 2 To UsedRange.Columns.Count
            If Trim(Cells(i, j).Value) = "" Then
                Cells(i, j).Value = "NA"
            End If
        Next
    Next
End Sub
