Option Explicit

Sub FilterAddress()
Rem:----------------------------------------------------
Dim Total, count As Integer
Total = UsedRange.Rows.count
Rem:----------------------------------------------------
Dim Directory, FilePath As String
Rem:############### SPECIFY THE FILEPATH HERE! ##############################################

'Directory = "E:\study\Spring 2010\STAT 511 (TA)"

Directory = ThisWorkbook.Path

FilePath = Directory & "\gfilter.txt"

Rem:#########################################################################################
Dim EmailAddress As String
Rem: read all emails addresses
EmailAddress = Cells(1, 1).Value
For count = 2 To Total
    EmailAddress = EmailAddress & " OR " & Cells(count, 1).Value
Next
Rem:-------- write the string into a file ----------------------
Open FilePath For Output As #1
Print #1, EmailAddress
Close #1
Rem:------------------------------------------------------------
End Sub
