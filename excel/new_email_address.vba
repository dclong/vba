Option Explicit

Sub NewEmailAddress()
    Rem:-----------------------------------------------------------------------------------------------------------------
    Const NewEmailIndex As String = "D"
    Const GmailSortIndex As String = "O"
    Rem:------------------------------------------------------------------
    Const GmailContact As String = "Gmail Contacts.xlsx"
    Rem:-----------------------------------------------------------------------------------------------------------------
    Dim GmailTotal, NewEmailTotal, count, counter, flag, InsertLocation As Integer
    Dim CellContent As String
    Rem:------------------------------------------------------------------
    Workbooks(GmailContact).Activate
'    Workbooks(GmailContact).Sheets(1).UsedRange.Columns.Sort key1:=Columns("O"), Header:=xlYes
'    Workbooks(GmailContact).Save
    GmailTotal = Workbooks(GmailContact).Sheets(1).UsedRange.Rows.count
    Rem:------------------------------------------------------------------
    ThisWorkbook.Activate
    Sheets(1).Activate
    UsedRange.Columns.Sort key1:=Columns(NewEmailIndex), Header:=xlYes
    NewEmailTotal = UsedRange.Rows.count
    Rem:------------------------------------------------------------------
    flag = 2
    InsertLocation = 2
    Rem:-------------------------
    For count = 2 To NewEmailTotal
        CellContent = Trim(Cells(count, NewEmailIndex))
        Cells(count, NewEmailIndex) = CellContent
        CellContent = LCase(CellContent)
        For counter = flag To GmailTotal
            If LCase(Workbooks(GmailContact).Sheets(1).Cells(counter, GmailSortIndex).Value) = CellContent Then
               flag = counter
               GoTo Line1
            End If
        Next counter
        Rem:----------- this is a new email address ----------------------------------------
        Rows(count).Copy Sheets(2).Rows(InsertLocation)
        Rows(count).Font.ColorIndex = 3
        InsertLocation = InsertLocation + 1
Line1:
    Next count
    Rem:---------------------------------------------------------------------------------------------------------
    Rows(1).Copy Sheets(2).Rows(1)
    ThisWorkbook.Save
    Rem:--------------------------------------------------------------------------------------------------------
End Sub
