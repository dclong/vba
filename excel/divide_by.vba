Sub DivideBy()
   Rem: This function is used to divide the numbers in the sheet by a given divisor
   Rem: ###################################################################################
   Dim divisor As Single
   divisor = 10
   Rem: ###################################################################################
   Dim rowstart, rownum, colstart, colnum As Integer
   rowstart = Selection.Row
   colstart = Selection.Column
   rownum = Selection.Rows.count
   colnum = Selection.Columns.count
   Dim i, j As Integer
   For i = 1 To rownum
      For j = 1 To colnum
         Cells(rowstart + i - 1, colstart + j - 1) = Cells(rowstart + i - 1, colstart + j - 1) / 10
      Next
   Next
End Sub