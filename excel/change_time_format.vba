
Option Explicit

Sub ChangeTimeFormat()
    Rem:--------------------------------------
    Dim flag As String
    Rem:--------------------------------------
    Dim SheetNum, RowNum, ColNum As Integer
    Rem:--------------------------------------
    SheetNum = ThisWorkbook.Sheets.Count
    Rem:---------------------------------------
    Dim SheetCount, RowCount, ColCount As Integer
    Dim MaxTime, TempTime As Double
    
    For SheetCount = 1 To SheetNum
        ThisWorkbook.Sheets(SheetCount).Activate
        ThisWorkbook.Sheets(SheetCount).UsedRange.Select
        
        Selection.Replace What:="+", Replacement:=" ", _
        LookAt:=xlPart, SearchOrder:=xlByRows, MatchCase:=False, SearchFormat:= _
        False, ReplaceFormat:=False
        
        Selection.Replace What:="-", Replacement:=" ", _
        LookAt:=xlPart, SearchOrder:=xlByRows, MatchCase:=False, SearchFormat:= _
        False, ReplaceFormat:=False
        
        With Selection
           .NumberFormatLocal = "0.00_);[��ɫ](0.00)"
           .HorizontalAlignment = xlCenter
           .VerticalAlignment = xlCenter
           .WrapText = False
           .Orientation = 0
           .AddIndent = False
           .IndentLevel = 0
           .ShrinkToFit = False
           .ReadingOrder = xlContext
           .MergeCells = False
        End With
        
        
        RowNum = ThisWorkbook.Sheets(SheetCount).UsedRange.Rows.Count
        ColNum = ThisWorkbook.Sheets(SheetCount).UsedRange.Columns.Count
        For ColCount = 2 To ColNum
            MaxTime = 0
            For RowCount = 2 To RowNum
               flag = Sheets(SheetCount).Cells(RowCount, ColCount).Value
               flag = Trim(flag)
               If InStr(1, flag, "Extra") > 0 Then
                  GoTo Line1
               Else
                      If flag = "" Then
                         GoTo Line1
                      End If
                      
               End If
    
               TempTime = CDbl(flag)
               If TempTime > MaxTime Then
                  MaxTime = TempTime
               Else
                  TempTime = TempTime + 0.5
                  MaxTime = TempTime
                  Rem:-- write data into file
                  Sheets(SheetCount).Cells(RowCount, ColCount).Value = TempTime
               End If
Line1:
            Next
        Next
    Next
    
    
End Sub






