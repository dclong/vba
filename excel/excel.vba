Option Explicit

Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Declare Function CloseClipboard Lib "User32" () As Long
Declare Function OpenClipboard Lib "User32" (ByVal hwnd As Long) As Long
Declare Function EmptyClipboard Lib "User32" () As Long
Declare Function lstrcpy Lib "kernel32" (ByVal lpString1 As Any, ByVal lpString2 As Any) As Long
Declare Function SetClipboardData Lib "User32" (ByVal wFormat As Long, ByVal hMem As Long) As Long
Public Const GHND = &H42
Public Const CF_TEXT = 1
Public Const MAXSIZE = 4096

Private Sub ClipBoard_SetData(MyString As String)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Put the argument string into the clipboard.
' @param:
' @author:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   Dim hGlobalMemory As Long, lpGlobalMemory As Long
   Dim hClipMemory As Long, X As Long
   ' Allocate moveable global memory.
   '-------------------------------------------
   hGlobalMemory = GlobalAlloc(GHND, Len(MyString) + 1)
   ' Lock the block to get a far pointer
   ' to this memory.
   lpGlobalMemory = GlobalLock(hGlobalMemory)
   ' Copy the string to this global memory.
   lpGlobalMemory = lstrcpy(lpGlobalMemory, MyString)
   ' Unlock the memory.
   If GlobalUnlock(hGlobalMemory) <> 0 Then
      MsgBox "Could not unlock memory location. Copy aborted."
      GoTo OutOfHere2
   End If
   ' Open the Clipboard to copy data to.
   If OpenClipboard(0&) = 0 Then
      MsgBox "Could not open the Clipboard. Copy aborted."
      Exit Sub
   End If
   ' Clear the Clipboard.
   X = EmptyClipboard()
   ' Copy the data to the Clipboard.
   hClipMemory = SetClipboardData(CF_TEXT, hGlobalMemory)
OutOfHere2:
   If CloseClipboard() = 0 Then
      MsgBox "Could not close Clipboard."
   End If
End Sub

Sub DataMarkdownTable()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Generate Markdown table code from the selected range.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim rng As Range
    Set rng = Selection
    Dim s As String
    ' first row as header
    s = MarkdownOneRow(rng.Rows(1), True)
    Dim nrow, i As Integer
    nrow = rng.Rows.Count
    For i = 2 To nrow
        s = s & vbCrLf & MarkdownOneRow(rng.Rows(i), False)
    Next
    ClipBoard_SetData s
End Sub

Private Function MarkdownOneRow(ByRef rng As Range, ByVal header As Boolean)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Generates Markdown code for 1 row of cells.
' This is a help function for MarkdownTable.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim s, h As String
    s = "|"
    h = "|"
    Dim ncol, i As Integer
    ncol = rng.Columns.Count
    For i = 1 To ncol
        s = s & rng.Cells(1, i).Value & "|"
        h = h & ":" & String(Len(rng.Cells(1, i).Value), "-") & ":" & "|"
    Next
    If header Then
        MarkdownOneRow = s & vbCrLf & h
    Else
        MarkdownOneRow = s
    End If
End Function


Sub DataReshapeArray()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Reshape a selected range, i.e., change the dimensions of the selected range.
' The value are read from and written to cells by row.
' The source range is truncated or extended using blank values automatically
' depends on whether contains more or fewer cells than the destination range.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim srcRange, desRange As Range
    ' record selected values in an array
    Set srcRange = Selection
    Dim values() As Variant
    ReDim values(srcRange.Count)
    Dim index As Integer
    index = 1
    Dim cell As Range
    For Each cell In srcRange
        values(index) = cell.Value
        index = index + 1
    Next cell
    ' ask for a output range
    On Error GoTo line1
    Set desRange = Application.InputBox(Prompt:="Please select destination range", Title:="Range Selection", Type:=8)
    index = 1
    For Each cell In desRange
        cell.Value = values(index)
        index = index + 1
    Next cell
line1:
End Sub

Sub DataCountNumberOfCells()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Count the number of selected cells.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim srcRange As Range
    Set srcRange = Selection
    MsgBox "The number of selected cells is: " & srcRange.Cells.Count & ".", , "Count Cells"
End Sub

Sub FormatBorderSSIDSF()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Border the selected cells using single solid for inner aera (SSI) and double solid lines for frame (DSF).
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    With Selection.Borders(xlEdgeLeft)
        .LineStyle = xlDouble
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThick
    End With
    With Selection.Borders(xlEdgeTop)
        .LineStyle = xlDouble
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThick
    End With
    With Selection.Borders(xlEdgeBottom)
        .LineStyle = xlDouble
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThick
    End With
    With Selection.Borders(xlEdgeRight)
        .LineStyle = xlDouble
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThick
    End With
    With Selection.Borders(xlInsideVertical)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
    With Selection.Borders(xlInsideHorizontal)
        .LineStyle = xlContinuous
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
        .Weight = xlThin
    End With
End Sub

Sub FormatCommaSeparatedZeroDecimal()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Format numbers in selected cells to be comma separated and no decimals.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.NumberFormat = "#,##0"
End Sub

Sub FormatZeroDecimalNumber()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Format numbers in selected cells to have no decimal
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.NumberFormat = "0"
End Sub

Sub FormatNoDecimalNumber()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Format numbers in selected cells to have no decimal
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.NumberFormat = "0"
End Sub

Sub FormatOneDecimalNumber()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Format numbers in selected cells to have 1 decimal.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.NumberFormat = "0.0"
End Sub

Sub FormatTwoDecimalNumber()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' @description: Format numbers in selected cells to have 2 decimals.
' @param:
' @author: Benjamin Du
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Selection.NumberFormat = "0.00"
End Sub

Sub YAxisStartFromZero()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Change the minimum value of the Y-axis of the select chart to 0.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ActiveChart.Axes(xlValue).MinimumScale = 0
End Sub
