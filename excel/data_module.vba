Option Explicit

Sub reshape()
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Reshape a selected range, i.e., change the dimensions of the selected range.
' The value are read from and written to cells by row.
' The source range is truncated or extended using blank values automatically
' depends on whether contains more or fewer cells than the destination range.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Dim srcRange, desRange As Range
    ' record selected values in an array
    Set srcRange = Selection
    Dim values() As Variant
    ReDim values(srcRange.Count)
    Dim index As Integer
    index = 1
    Dim cell As Range
    For Each cell In srcRange
        values(index) = cell.Value
        index = index + 1
    Next cell
    ' ask for a output range
    On Error GoTo line1
    Set desRange = Application.InputBox(Prompt:="Please select destination range", Title:="Range Selection", Type:=8)
    Application.GoTo desRange
    index = 1
    For Each cell In desRange
        cell.Value = values(index)
        index = index + 1
    Next cell
line1:
End Sub
