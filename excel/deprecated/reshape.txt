Option Explicit

Sub Reshape()
        Rem:-----------------------------------------------------------------
        'I envolved in this project to help Meiguan Yan to clean data
        'Since I don't the original data now, I've no idea what the code does.
        Rem:------------------------------------------------------------------
        Dim tempStr As String
        Dim totalRowNumber, counter, currentIndex, columnIndex As Long
        Dim newSheet As Integer
        Rem:------------------------------------------------------------------
        ThisWorkbook.Sheets.Add after:=Sheets(Sheets.Count)
        newSheet = ThisWorkbook.Sheets.Count
        currentIndex = 1
        Rem: ----------- copy data in sheet 1 to the new sheet ------------------------------
        ' get the total number of rows in sheet1
        totalRowNumber = ThisWorkbook.Sheets(1).UsedRange.Rows.Count
        For counter = 1 To totalRowNumber
            'check wheter there's data
            If Not IsNull(ThisWorkbook.Sheets(1).Cells(counter, 1)) Then
               tempStr = ThisWorkbook.Sheets(1).Cells(counter, 1).Value
               tempStr = Trim(tempStr)
               If tempStr <> "" Then
                    'initialize column inde
                    columnIndex = 3
Line1:
                     ' get the content in the corresponding cell
                    If Not IsNull(ThisWorkbook.Sheets(1).Cells(counter, columnIndex)) Then
                        tempStr = ThisWorkbook.Sheets(1).Cells(counter, columnIndex)
                        tempStr = Trim(tempStr)
                        If tempStr <> "" Then
                            ' copy the facebook id to the new sheet at currentIndex
                            ThisWorkbook.Sheets(newSheet).Cells(currentIndex, 1) = ThisWorkbook.Sheets(1).Cells(counter, 1).Value
                           ThisWorkbook.Sheets(newSheet).Cells(currentIndex, 2) = ThisWorkbook.Sheets(1).Cells(counter, columnIndex).Value
                            columnIndex = columnIndex + 1
                            currentIndex = currentIndex + 1
                            GoTo Line1
                        End If
                    End If
               End If
            End If
        Next
        'turn off warnings
        Application.DisplayAlerts = False
        'delete the first sheet
        ThisWorkbook.Sheets(1).Delete
        'save the file
        ThisWorkbook.Save
        Application.DisplayAlerts = True
        Rem:--------------------------------------------------------------------------------
End Sub


