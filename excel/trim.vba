Option Explicit


Sub DataTrimUsedRange()
    Dim nrow, ncol, i, j As Integer
    nrow = ActiveSheet.UsedRange.Rows.Count
    ncol = ActiveSheet.UsedRange.Columns.Count
    For i = 1 To nrow
        For j = 1 To ncol
            Cells(i, j).Value = trim(Cells(i, j).Value)
        Next
    Next
End Sub

Sub DataTrimSelectedRange()
    Dim rng, cell As Range
    Set rng = Selection
    For Each cell In rng.Cells
        cell.Value = trim(cell.Value)
    Next
End Sub

