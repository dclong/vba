Sub FindSameRow()
    Rem:------------------------------------
    'Highlight duplicated rows in red
    Rem:------------------------------------
    Dim ColumnIndex As String
    ColumnIndex = "O"
    UsedRange.Columns.Sort key1:=Columns("O"), Header:=xlYes
    Dim nrow, count As Integer
    nrow = UsedRange.Rows.count
    For count = 2 To nrow
        If LCase(Cells(count, ColumnIndex).Value) = LCase(Cells(count - 1, ColumnIndex).Value) Then
            Rows(count).Font.ColorIndex = 3
        End If
    Next
End Sub