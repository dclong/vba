Sub Transpose()
   Rem:#######################################################################################
   Rem: This function is used to tanspose the selected range
   Rem: The left-top cell of the new postion is the same as the original
   Dim rowstart, rownum, colstart, colnum As Integer
   rowstart = Selection.Row
   colstart = Selection.Column
   rownum = Selection.Rows.count
   colnum = Selection.Columns.count
   Rem:--------------------------------------------------------------------------------------
   Rem: transpose the selected range
   Dim rng As Range
   Set rng = Range(Cells(rowstart, colstart), Cells(rowstart + colnum - 1, colstart + rownum - 1))
   rng.Value = WorksheetFunction.Transpose(Selection)
   Rem: clear some part if necessary
   Dim m, n As Integer
   n = Application.WorksheetFunction.Min(rownum, colnum)
   m = rownum + colnum - n
   If rownum > colnum Then
      Set rng = Range(Cells(rowstart + n, colstart), Cells(rowstart + m - 1, colstart + n - 1))
   Else
      Set rng = Range(Cells(rowstart, colstart + n), Cells(rowstart + n - 1, colstart + m - 1))
   End If
   rng.Clear
End Sub