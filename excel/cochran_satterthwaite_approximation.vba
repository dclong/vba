Rem: This program is used to calculate the Cochran-Satterthwaite Approximation.
Rem: Copyright @ Chuanlong Du
Option Explicit
Sub CochSatterApprox()
Rem: First find where is the end of the data
Dim MSnum As Integer
MSnum = 2
While Trim(Cells(MSnum, "A").Value) <> ""
      MSnum = MSnum + 1
Wend
MSnum = MSnum - 1
Dim Comb, Nu, temp As Single
Comb = 0 'the linear combination of the MS's
Nu = 0 'the degrees of freedom of the approximate Chisquare statistic
Dim count As Integer
For count = 2 To MSnum
    temp = Cells(count, "A").Value * Cells(count, "C").Value
    Comb = Comb + temp
    Nu = Nu + temp ^ 2 / Cells(count, "B").Value
Next
Nu = Comb ^ 2 / Nu
Dim alpha As Single
alpha = Cells(2, 4) 'significant level
Cells(2, 5).Value = Comb
Cells(2, 6).Value = Nu
Dim UpperQ, LowerQ As Single
Cells(2, 7).Formula = "=GAMMAINV(" & (1 - alpha / 2) & "," & (Nu / 2) & "," & 2 & ")" 'upper Chisquare quantile
UpperQ = Cells(2, 7).Value
Cells(2, 7).Clear
Cells(2, 7).Value = UpperQ
Cells(2, 8).Formula = "=GAMMAINV(" & (alpha / 2) & "," & (Nu / 2) & "," & 2 & ")" 'lower Chisquare quantile
LowerQ = Cells(2, 8).Value
Cells(2, 8).Clear
Cells(2, 8).Value = LowerQ
Dim UpperLimit, LowerLimit As Single
LowerLimit = Nu * Comb / UpperQ
UpperLimit = Nu * Comb / LowerQ
Rem: find which one on earth is the upper limit and calculate the confidence interval for S
Dim root1, root2 As Single
If UpperLimit > LowerLimit Then
   Rem: both positive
   Cells(2, 9).Value = LowerLimit
   Cells(2, 10).Value = UpperLimit
   Cells(3, 9).Formula = "=SQRT(" & LowerLimit & ")"
   root1 = Cells(3, 9).Value
   Cells(3, 9).Clear
   Cells(3, 9).Value = root1
   Cells(3, 10).Formula = "=SQRT(" & UpperLimit & ")"
   root2 = Cells(3, 10).Value
   Cells(3, 10).Clear
   Cells(3, 10).Value = root2
Else
   Rem: both are negative
   Cells(2, 9).Value = UpperLimit
   Cells(2, 10).Value = LowerLimit
   Cells(3, 9).Formula = "=SQRT(" & -UpperLimit & ")"
   root1 = -Cells(3, 9).Value
   Cells(3, 9).Clear
   Cells(3, 9).Value = root1
   Cells(3, 10).Formula = "=SQRT(" & -LowerLimit & ")"
   root2 = -Cells(3, 10).Value
   Cells(3, 10).Clear
   Cells(3, 10).Value = root2
End If
Rem: format the table
Cells(1, 5).Value = "Comb"
Cells(1, 6).Value = "Nu"
Cells(1, 7).Value = "ChiUpperQ"
Cells(1, 8).Value = "ChiLowerQ"
Cells(1, 9).Value = "LowerLimit"
Cells(1, 10).Value = "UpperLimit"
Cells(3, 5).Value = "Confidence Interval for S"
Range("E3", "H3").Select
With Selection
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = True
End With
Rem: add border to the table
Range("E1", "J3").Select
With Selection
     .Borders.LineStyle = 1
     .Columns.AutoFit
End With
End Sub
