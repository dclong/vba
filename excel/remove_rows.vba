Sub RemoveRows()
    Rem:----------------------------------------------
    'Delete rows that satisfy an creiteria
    'Rows are deleted from back to improve performance
    Rem:----------------------------------------------
    For i = UsedRange.Rows.Count To 2 Step -1
        If Cells(i, "D").Value <> "EDF" Then
            Rows(i).Delete
        End If
    Next
End Sub
