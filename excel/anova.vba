Option Explicit
Sub ANOVA()
Rem: first find where is the end of the data
Dim trtnum As Integer
trtnum = 2
While Trim(Cells(trtnum, "A").value) <> ""
    trtnum = trtnum + 1
Wend
trtnum = trtnum - 2
Rem: calculate the Sample variance
If Trim(Cells(2, "D").value) = "" Then
    Dim count As Integer
    Dim std As Single
    For count = 1 To trtnum
        std = Cells(count + 1, "C")
        Cells(count + 1, "D").value = std * std
    Next
End If
Rem: clear the area where is going to be use to display ANOVA table
Range(Cells(trtnum + 2, "A"), Cells(trtnum + 100, "Z")).Clear
Rem: Build up the ANOVA table
Cells(trtnum + 4, "A").value = "Source"
Cells(trtnum + 4, "B").value = "D.F."
Cells(trtnum + 4, "C").value = "SS"
Cells(trtnum + 4, "D").value = "MS"
Cells(trtnum + 4, "E").value = "F"
Cells(trtnum + 4, "F").value = "Pvalue"
Cells(trtnum + 5, "A").value = "TRT"
Cells(trtnum + 6, "A").value = "Error"
Cells(trtnum + 7, "A").value = "Total"
Rem: build the bouders for the table
Range(Cells(trtnum + 4, "A"), Cells(trtnum + 7, "F")).Borders.LineStyle = 1
Rem: calculate the statistics
Cells(trtnum + 5, "B").value = trtnum - 1
Dim NumTotal, DFTotal As Integer
NumTotal = 0
For count = 1 To trtnum
    NumTotal = NumTotal + Cells(count + 1, "E")
Next
DFTotal = NumTotal - 1
Cells(trtnum + 7, "B").value = DFTotal
Cells(trtnum + 6, "B").value = DFTotal - trtnum + 1
Dim SumTotal As Single
SumTotal = 0
For count = 1 To trtnum
    SumTotal = SumTotal + Cells(count + 1, "B") * Cells(count + 1, "E")
Next
Dim SSTRT, ysqsum As Single
SSTRT = 0
For count = 1 To trtnum
    SSTRT = SSTRT + Cells(count + 1, "B").value ^ 2 * Cells(count + 1, "E")
Next
ysqsum = SSTRT
SSTRT = SSTRT - SumTotal / NumTotal * SumTotal
Cells(trtnum + 5, "C").value = SSTRT
Dim SSError As Single
SSError = 0
For count = 1 To trtnum
    SSError = SSError + Cells(count + 1, "D").value * (Cells(count + 1, "E").value - 1)
Next
Cells(trtnum + 6, "C").value = SSError
Cells(trtnum + 7, "C").value = SSError + SSTRT
Rem: calculate mean squares
Cells(trtnum + 5, "D").value = Cells(trtnum + 5, "C").value / Cells(trtnum + 5, "B").value
Cells(trtnum + 6, "D").value = Cells(trtnum + 6, "C").value / Cells(trtnum + 6, "B").value
Cells(trtnum + 7, "D").value = Cells(trtnum + 7, "C").value / Cells(trtnum + 7, "B").value
Rem: calculate F statistics
Cells(trtnum + 5, "E").value = Cells(trtnum + 5, "D").value / Cells(trtnum + 6, "D").value
Cells(trtnum + 5, "G").Formula = "=FDIST(" & Cells(trtnum + 5, "E").value _
                & "," & Cells(trtnum + 5, "B").value & "," & Cells(trtnum + 6, "B").value & ")"
Cells(trtnum + 5, "F").value = Cells(trtnum + 5, "G").value
Cells(trtnum + 5, "G").Clear
Rem: Build another ANOVA table
Cells(trtnum + 10, "A").value = "Source"
Cells(trtnum + 10, "B").value = "D.F."
Cells(trtnum + 10, "C").value = "SS"
Cells(trtnum + 10, "D").value = "MS"
Cells(trtnum + 10, "E").value = "F"
Cells(trtnum + 10, "F").value = "Pvalue"
Cells(trtnum + 11, "A").value = "Regression"
Cells(trtnum + 12, "A").value = "Residual"
Cells(trtnum + 13, "A").value = "Lack of fit"
Cells(trtnum + 14, "A").value = "Pure Error"
Cells(trtnum + 15, "A").value = "Total"
Rem: build up borders for the table
Range(Cells(trtnum + 10, "A"), Cells(trtnum + 15, "F")).Borders.LineStyle = 1
Rem: calculate the statistics
Cells(trtnum + 11, "B") = 1
Cells(trtnum + 14, "B").value = Cells(trtnum + 6, "B")
Cells(trtnum + 14, "C").value = Cells(trtnum + 6, "C")
Cells(trtnum + 14, "D").value = Cells(trtnum + 6, "D")
Cells(trtnum + 12, "B").value = DFTotal - 1
Cells(trtnum + 15, "B").value = Cells(trtnum + 7, "B").value
Cells(trtnum + 15, "C").value = Cells(trtnum + 7, "C").value
Cells(trtnum + 15, "D").value = Cells(trtnum + 7, "D").value
Cells(trtnum + 13, "B").value = trtnum - 2

Rem: fit the linear model
Rem: first calculate the statistics for x
Dim xsum, xmean, xsqsum, xvar, xstd As Single
xsum = 0
xsqsum = 0
For count = 1 To trtnum
    xsum = xsum + Cells(count + 1, "A").value * Cells(count + 1, "E").value
    xsqsum = xsqsum + Cells(count + 1, "A").value * Cells(count + 1, "A").value * Cells(count + 1, "E").value
Next
xmean = xsum / NumTotal
xvar = (xsqsum - NumTotal * xmean * xmean) / NumTotal
Cells(trtnum + 16, "A").Formula = "=SQRT(" & xvar & ")"
xstd = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Clear
Rem: calculate the statistics for y
Dim ysum, ymean, yvar, ystd As Single
ysum = SumTotal
ymean = ysum / NumTotal
ysqsum = ysqsum + SSError
yvar = (ysqsum - NumTotal * ymean * ymean) / NumTotal
Cells(trtnum + 16, "A").Formula = "=SQRT(" & yvar & ")"
ystd = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Clear
Rem: calculate the statistics between x and y
Dim xysum, xymean As Single
xysum = 0
For count = 1 To trtnum
    xysum = xysum + Cells(count + 1, "A") * Cells(count + 1, "B") * Cells(count + 1, "E")
Next
xymean = xysum / NumTotal
Dim covxy As Single
covxy = xymean - xmean * ymean
Rem: calculate the statistics for the regression model
Dim b0, b1 As Single
b1 = covxy / xvar
b0 = ymean - b1 * xmean
Rem: build up the regression coefficient table
Cells(trtnum + 17, "A").value = "Parameters"
Cells(trtnum + 17, "B").value = "Estimation"
Cells(trtnum + 17, "C").value = "Standard Deviation"
Cells(trtnum + 17, "D").value = "T-statistics"
Cells(trtnum + 17, "E").value = "Pvalue"
Cells(trtnum + 18, "A").value = "Intercept"
Cells(trtnum + 19, "A").value = "X"
Range(Cells(trtnum + 17, "A"), Cells(trtnum + 19, "E")).Borders.LineStyle = 1
Cells(trtnum + 18, "B").value = b0
Cells(trtnum + 19, "B").value = b1
Rem: calculate the sum square of the regression model
Dim SSreg As Single
SSreg = 0
Dim i, j As Integer
For i = 1 To trtnum
    For j = 1 To Cells(i + 1, "E").value
        SSreg = SSreg + (b0 + b1 * Cells(i + 1, "A").value - ymean) ^ 2
    Next
Next
Cells(trtnum + 11, "C").value = SSreg
Cells(trtnum + 12, "c").value = Cells(trtnum + 15, "c") - SSreg
Cells(trtnum + 13, "c").value = SSTRT - SSreg
Rem: calculate the mean squares of the regression model
Cells(trtnum + 11, "d").value = Cells(trtnum + 11, "c").value / Cells(trtnum + 11, "b").value
Cells(trtnum + 12, "d").value = Cells(trtnum + 12, "c").value / Cells(trtnum + 12, "b").value
Cells(trtnum + 13, "d").value = Cells(trtnum + 13, "c").value / Cells(trtnum + 13, "b").value
Rem: calculate the F-statistics for the regression model
Cells(trtnum + 11, "e").value = Cells(trtnum + 11, "d").value / Cells(trtnum + 14, "d").value
Cells(trtnum + 13, "e").value = Cells(trtnum + 13, "d").value / Cells(trtnum + 14, "d").value
Rem: calculate the p-value
Cells(trtnum + 16, "A").Formula = "=FDIST(" & Cells(trtnum + 11, "e").value & ",1," _
                          & Cells(trtnum + 14, "b").value & ")"
Cells(trtnum + 11, "f").value = Cells(trtnum + 16, "a").value
Cells(trtnum + 16, "A").Formula = "=FDIST(" & Cells(trtnum + 13, "e").value & "," _
                          & Cells(trtnum + 13, "b").value & "," & Cells(trtnum + 14, "b").value & ")"
Cells(trtnum + 13, "f").value = Cells(trtnum + 16, "a").value
Cells(trtnum + 16, "a").Clear
Rem: calcualte the standard deviation of the coefficients
Dim b0var, b1var, b0std, b1std As Single
b0var = xsqsum / NumTotal / NumTotal * Cells(trtnum + 12, "D").value / xvar
b1var = 1 / NumTotal * Cells(trtnum + 12, "D").value / xvar
Cells(trtnum + 16, "A").Formula = "=SQRT(" & b0var & ")"
b0std = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Formula = "=SQRT(" & b1var & ")"
b1std = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Clear
Cells(trtnum + 18, "C").value = b0std
Cells(trtnum + 19, "C").value = b1std
Rem: calculate the T-statistics
Cells(trtnum + 18, "D").value = Cells(trtnum + 18, "B").value / Cells(trtnum + 18, "C").value
Cells(trtnum + 19, "D").value = Cells(trtnum + 19, "B").value / Cells(trtnum + 19, "C").value
Rem: calculate the p-value for the coefficients
Cells(trtnum + 16, "A").Formula = "=TDIST(" & "ABS(" & Cells(trtnum + 18, "D").value & ")," _
                        & Cells(trtnum + 14, "B").value & "," & "2)"
Cells(trtnum + 18, "E") = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Formula = "=TDIST(" & "ABS(" & Cells(trtnum + 19, "D").value & ")," _
                        & Cells(trtnum + 14, "B").value & "," & "2)"
Cells(trtnum + 19, "E") = Cells(trtnum + 16, "A").value
Cells(trtnum + 16, "A").Clear
End Sub
