Option Explicit

Sub GmailInbox()
Rem:-------------------------------------------
Dim count As Integer
Dim content, InboxStr, Directory, FilePath As String
Rem:-------------------------------------------
Rem:################################################## SPECIFY FILEPATH HERE! ############################################################################

Directory = ThisWorkbook.Path

Rem:--------------------------------------------------

FilePath = Directory & "\ginbox.txt"

Rem:#########################################################################################################################################################
Rem:-------- Read email addresses in the "From" column ----------
count = 2
content = Trim(Cells(count, 1).Value)
If content <> "" Then
    If InStr(1, content, "@") > 0 Then
        InboxStr = "From: " & content
    Else
        MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
        Exit Sub
    End If
    count = 3
    content = Trim(Cells(count, 1).Value)
    Do While (content <> "")
        If InStr(1, content, "@") > 0 Then
            InboxStr = InboxStr & " OR From: " & content
        Else
            MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
            Exit Sub
        End If
    Loop
    Rem:------- Read email addresses in the "To" column ---------------
    count = 2
    content = Trim(Cells(count, 2).Value)
    If content <> "" Then
        If InStr(1, content, "@") > 0 Then
            InboxStr = "(" & InboxStr & ")"
            InboxStr = InboxStr & " AND (To: " & content
            Rem: ----- read email addresses ----------------------
            count = 3
            content = Trim(Cells(count, 2).Value)
            Do While (content <> "")
                If InStr(1, content, "@") > 0 Then
                    InboxStr = InboxStr & " OR To:" & content
                Else
                    MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
                    Exit Sub
                End If
            Loop
            InboxStr = InboxStr & ")"
        Else
            MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
            Exit Sub
        End If
    End If
Else
    Rem: deal with To clause
    count = 2
    content = Trim(Cells(count, 2).Value)
    If content <> "" Then
        If InStr(1, content, "@") > 0 Then
            InboxStr = "To: " & content
        Else
            MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
            Exit Sub
        End If
        count = 3
        content = Trim(Cells(count, 2).Value)
        Do While (content <> "")
            If InStr(1, content, "@") > 0 Then
                InboxStr = InboxStr & "To: " & content
            Else
                MsgBox "The email address in row " & count & " is illegal! Please correct it and run this program again!", vbOKOnly, "Illegal Email Address:"
                Exit Sub
            End If
        Loop
    Else
        MsgBox "No email address found! Please enter email addresses and run this program again", vbOKOnly, "No email address found:"
        Exit Sub
    End If
End If
Rem:----------------------- write the string into a file ------------------------------------
Open FilePath For Output As #1
Print #1, InboxStr
Close #1
Rem:------------------------------------------------------------------------------------------
End Sub
