Option Explicit

Sub ShiftRight()
    Dim TotalRowNumber, i As Long
    Dim tempStr As String
    TotalRowNumber = UsedRange.Rows.Count
    For i = 1 To TotalRowNumber
        If IsNull(Cells(i, "I")) Then
            'move
            Cells(i, "I") = Cells(i, "H")
            Cells(i, "H") = Cells(i, "G")
            Cells(i, "G") = Cells(i, "F")
            Cells(i, "F") = Cells(i, "E")
            Cells(i, "E") = ""
        Else
            tempStr = Cells(i, "I").Value
            tempStr = Trim(tempStr)
            If tempStr = "" Then
                 'move
                Cells(i, "I") = Cells(i, "H")
                Cells(i, "H") = Cells(i, "G")
                Cells(i, "G") = Cells(i, "F")
                Cells(i, "F") = Cells(i, "E")
                Cells(i, "E") = ""
            End If
        End If
    Next
End Sub


