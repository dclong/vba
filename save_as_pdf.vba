Sub SaveAsPDF()
    ChangeFileOpenDirectory _
        "E:\Study\Fall 2009\STAT 500 (TA)\Homework\HW3\Electronic Submission\"
    ActiveDocument.ExportAsFixedFormat OutputFileName:= _
        "E:\Study\Fall 2009\STAT 500 (TA)\Homework\HW3\Electronic Submission\stat500hw2_nikolova-2.pdf" _
        , ExportFormat:=wdExportFormatPDF, OpenAfterExport:=True, OptimizeFor:= _
        wdExportOptimizeForPrint, Range:=wdExportAllDocument, From:=1, To:=1, _
        Item:=wdExportDocumentContent, IncludeDocProps:=True, KeepIRM:=True, _
        CreateBookmarks:=wdExportCreateNoBookmarks, DocStructureTags:=True, _
        BitmapMissingFonts:=True, UseISO19005_1:=False
End Sub
