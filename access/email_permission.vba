Sub EmailPermission(ByVal permission As Boolean)
   Rem: ######################################################################################################################################

   Dim SqlStr As String
   Rem: ######################################################################################################################################
'   Access.DoCmd.SetWarnings (False)
   Rem: -----------------------------------------------------------------------------------------------------
   If permission Then
       SqlStr = "update " & AllMyContacts & " set NoSkip=1"
   Else
       SqlStr = "update " & AllMyContacts & " set NoSkip=0"
   End If
   DoCmd.RunSQL SqlStr
   Rem:-------------------------------------------------------------------------------------------------------
'   Access.DoCmd.SetWarnings (True)
End Sub