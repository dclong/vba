Option Compare Database

Option Explicit

Sub TryEmail()
   Rem: --------------------------------------------------------------------------
   Dim item As Outlook.MailItem
   Dim OutObj As Outlook.Application
   Dim EmailAddress, EmailSubject, EmailBody, FilePath As String
   Rem: --------------------------------------------------------------------------
   
   EmailAddress = "dclong@iastate.edu"
   
   EmailSubject = "html configuration!"
   
   FilePath = "D:\Contacts\Remind\areminder7.txt"
   
   EmailBody = GetFileContent(FilePath)
   
   'EmailBody = "<HTML><H2>The   body   of   this   message   will   appear   in   HTML.</H2><BODY>Please   enter   the   message   text   here.   </BODY></HTML>"
   
   Set OutObj = New Outlook.Application
   
   Set item = OutObj.CreateItem(olMailItem)
   
   item.To = EmailAddress
   
   item.Subject = EmailSubject
   
   'Item.Attachments.Add ("d:\study\mail.txt")
             
    'item.Body = EmailBody
             
   item.HTMLBody = EmailBody
             
   item.importance = Outlook.OlImportance.olImportanceHigh
             
   item.Send
             
   Set item = Nothing

End Sub


Function GetFileContent(ByVal FilePath As String)
    GetFileContent = ""
    Dim file As Long
    file = FreeFile()
    On Error GoTo ExitFun
    Open FilePath For Input As file
    Dim StrAll, StrLine As String
    StrAll = ""
    StrLine = ""
    Do While Not EOF(file)
       Line Input #file, StrLine
       If StrLine = "" Then
          StrLine = " "
       End If
       StrAll = StrAll & StrLine & vbCrLf
    Loop
    Close file
    GetFileContent = StrAll
ExitFun:
End Function
